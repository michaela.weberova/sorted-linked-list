import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortedLinkedListTest {

    @Test
    public void testContains_emptyList() {
        SortedLinkedList<Integer> linkedList = new SortedLinkedList<>();
        Assert.assertFalse(linkedList.contains(1));
    }

    @Test
    public void testAdd_addToEmptyList() {
        SortedLinkedList<Integer> linkedList = new SortedLinkedList<>();
        Assert.assertTrue(linkedList.isEmpty());
        linkedList.add(1);
        Assert.assertTrue(linkedList.contains(1));
        Assert.assertFalse(linkedList.isEmpty());
    }

    @Test
    public void testAdd_addMultipleEl() {
        SortedLinkedList<Integer> linkedList = new SortedLinkedList<>();
        List<Integer> values = Arrays.asList(1, 2, -1, 3);
        Assert.assertTrue(linkedList.isEmpty());
        linkedList.add(values);
        for (Integer value : values) {
            Assert.assertTrue(linkedList.contains(value));
            Assert.assertFalse(linkedList.isEmpty());
        }
    }


    @Test
    public void testDelete_firstElement() {
        SortedLinkedList<Integer> linkedList = new SortedLinkedList<>();
        linkedList.add(Arrays.asList(1, 2, 3));
        linkedList.delete(1);
        Assert.assertFalse(linkedList.contains(1));
    }

    @Test
    public void testDelete_LastElement() {
        SortedLinkedList<Integer> linkedList = new SortedLinkedList<>();
        linkedList.add(Arrays.asList(1, 2, 3));
        linkedList.delete(3);
        Assert.assertFalse(linkedList.contains(3));
    }

    @Test
    public void testDelete_MiddleElement() {
        SortedLinkedList<Integer> linkedList = new SortedLinkedList<>();
        linkedList.add(Arrays.asList(1, 2, 3));
        linkedList.delete(2);
        Assert.assertFalse(linkedList.contains(2));
    }

    @Test
    public void testDelete_OnlyElement() {
        SortedLinkedList<Integer> linkedList = new SortedLinkedList<>();
        linkedList.add(Collections.singletonList(1));
        linkedList.delete(1);
        Assert.assertFalse(linkedList.contains(1));
        Assert.assertTrue(linkedList.isEmpty());
    }

    @Test
    public void testDelete_NonExistingElement() {
        SortedLinkedList<Integer> linkedList = new SortedLinkedList<>();
        linkedList.add(Collections.singletonList(1));
        Assert.assertFalse(linkedList.delete(2));
    }
}
