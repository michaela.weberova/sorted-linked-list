import java.util.List;

/**
 * Sorted linked list.
 *
 * @param <T> type of the elements stored in the linked list
 */
public class SortedLinkedList<T extends Comparable<T>> {

    private Node head = null;

    private class Node {
        public Node(Node next, T value) {
            this.next = next;
            this.value = value;
        }

        public Node next;
        public T value;
    }

    /**
     * @return true if list is empty, otherwise false
     */
    public boolean isEmpty() {
        return head == null;
    }

    /**
     * Deletes element from the list.
     *
     * @param element element to be deleted
     * @return true if the element was deleted, false if the item is not found
     */
    public boolean delete(T element) {
        Node prev = null;
        Node tmp = head;

        while (tmp != null) {
            if (tmp.value == element) {
                if (prev == null && tmp.next != null) {
                    head = tmp.next;
                } else if (prev == null) {
                    head = null;
                } else {
                    prev.next = tmp.next;
                }
                return true;
            }
            prev = tmp;
            tmp = tmp.next;
        }

        return false;
    }

    /**
     * Adds multiple elements into the list, calls on each element {@link #add(T) add}.
     *
     * @param elements elements to be added
     */
    public void add(List<T> elements) {
        for (T element : elements) {
            add(element);
        }
    }

    /**
     * Adds single element into the list.
     *
     * @param element element to be added
     */
    public void add(T element) {
        if (head == null) {
            head = new Node(null, element);
            return;
        }

        Node prev = null;
        Node curr = head;

        while (curr != null) {
            int res = element.compareTo(curr.value);
            if (res > 0) {
                prev = curr;
                curr = curr.next;
            } else if (prev != null) {
                prev.next = new Node(curr, element);
                return;
            } else {
                head = new Node(head, element);
                return;
            }
        }

        prev.next = new Node(null, element);
    }

    /**
     * @param element searched element
     * @return true if element is found, otherwise false
     */
    public boolean contains(T element) {
        Node tmp = head;
        while (tmp != null) {
            if (tmp.value == element) {
                return true;
            }
            tmp = tmp.next;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        Node tmp = head;
        while (tmp != null) {
            res.append(tmp.value).append(" ");
            tmp = tmp.next;
        }
        return res.toString();
    }
}
