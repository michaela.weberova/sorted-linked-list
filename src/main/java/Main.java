import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // Example of sorted linked list with integer values
        SortedLinkedList<Integer> integerSortedLinkedList = new SortedLinkedList<>();
        integerSortedLinkedList.add(Arrays.asList(1, 2, 3, -6, 10, 0));
        System.out.println(integerSortedLinkedList);
        integerSortedLinkedList.delete(1);
        integerSortedLinkedList.delete(-6);
        System.out.println(integerSortedLinkedList);

        // Example of sorted linked list with string values
        SortedLinkedList<String> stringSortedLinkedList = new SortedLinkedList<>();
        stringSortedLinkedList.add(Arrays.asList("a", "b", "ab", "cb",  "ca"));
        System.out.println(stringSortedLinkedList);
        stringSortedLinkedList.delete("a");
        stringSortedLinkedList.delete("ca");
        System.out.println(stringSortedLinkedList);
    }
}
